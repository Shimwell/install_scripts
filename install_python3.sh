sudo apt-get update
sudo apt install build-essential -y
sudo chown $USER /opt
wget https://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh
bash Anaconda3-4.4.0-Linux-x86_64.sh -b -p /opt/anaconda3
rm Anaconda*
echo 'export PATH="$PATH:/opt/anaconda3/bin"' >> ~/.bashrc
bash
conda --version
conda config --add channels conda-forge
conda create -n app python -y
source activate app
conda install scipy numpy scikit-learn flask flask-cors uwsgi xgboost PySide -y