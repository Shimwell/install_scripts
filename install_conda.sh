#!/bin/bash

cd
current_dir=$(pwd)

cd /tmp
wget https://repo.continuum.io/archive/Anaconda3-4.4.0-Linux-x86_64.sh
bash Anaconda3-4.4.0-Linux-x86_64.sh -b -p $HOME/anaconda3

export PATH=$current_dir/anaconda3/bin:$PATH
source ~/.bashrc
conda --version
rm Anaconda3-4.4.0-Linux-x86_64.sh
