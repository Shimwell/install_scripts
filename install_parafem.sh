# ubuntu 16.04 lts
sudo apt-get update

echo "Installing SVN"
sudo apt install -y subversion
#sudo apt install libopenmpi-dev openmpi-bin -f #does not work 
echo "Purging MPI"
sudo apt-get purge mpi mpich openmpi-common -y
echo "Installing mpich"
sudo apt-get install -y mpich
echo "Installing gfortran"
sudo apt install gfortran -f
echo "Installing make"
sudo apt install -y make

echo "Checking out ParaFEM"
svn checkout http://svn.code.sf.net/p/parafem/code/trunk parafem-code

echo "Making ParaFEM"
current_dir=$(pwd)
cd parafem-code/parafem
#./make-parafem MACHINE=linuxdesktop clean
#./make-parafem MACHINE=linuxdesktop execlean
./make-parafem MACHINE=linuxdesktop

echo "Add ParaFEM to bashrc"
echo 'export PATH="'$current_dir'/parafem-code/parafem/bin:$PATH"' >> ~/.bashrc
export PATH="$current_dir/parafem-code/parafem/bin:$PATH"


echo "Test install with p121 code"
cd $current_dir
#cd parafem-code/parafem/examples/5th_ed/p121/demo
cp -r parafem-code/parafem/examples/5th_ed/p121/demo .
cd demo
rm p121_demo.res
mpirun -np 2 p121 p121_demo
cat p121_demo.res
cd ../
rm -r demo


echo "all completed don't forget to source your ~/.bashrc"


