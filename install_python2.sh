sudo apt-get update
sudo apt install build-essential -y
sudo chown $USER /opt
wget https://repo.continuum.io/archive/Anaconda2-4.4.0-Linux-x86_64.sh
bash Anaconda2-4.4.0-Linux-x86_64.sh -b -p /opt/anaconda2
rm Anaconda*
echo 'export PATH="$PATH:/opt/anaconda2/bin"' >> ~/.bashrc
echo 'PATH="'"$PATH"':/opt/anaconda2/bin"' | sudo tee /etc/environment

#bash
conda --version
conda config --add channels conda-forge

# conda create -n app python -y
# source activate app
# conda install scipy numpy scikit-learn flask flask-cors uwsgi xgboost PySide -y
