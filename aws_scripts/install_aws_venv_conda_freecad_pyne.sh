sudo apt-get update
sudo apt install build-essential -y
wget https://repo.continuum.io/archive/Anaconda2-4.4.0-Linux-x86_64.sh
bash Anaconda2-4.4.0-Linux-x86_64.sh -b -p $HOME/anaconda2
rm Anaconda*
echo 'export PATH="$PATH:$HOME/anaconda2/bin"' >> .bashrc
bash
conda --version
conda config --add channels conda-forge
conda create -n app python -y
source activate app
conda install scipy numpy scikit-learn flask flask-cors uwsgi xgboost PySide -y
conda install -c conda-forge pyne -y
mkdir app/
cd app/
wget -O app.py http://jshimwell.com/ssh/app.pyrename
wget -O uwsgi.ini http://jshimwell.com/ssh/uwsgi.ini
uwsgi --ini uwsgi.ini
conda env export > environment.yml
source deactivate