#!/bin/bash

chmod 700 duck.sh

mkdir ~/duckdns

cd ~/install_scripts/aws_scripts/make_webserver
chmod +x duck_daemon.sh
sudo chown root duck_daemon.sh
sudo chmod 744 duck_daemon.sh

# Test running
sudo ./duck_daemon.sh
ps -ef | grep duck

# Add to autostart on boot
sudo ln -s ~/install_scripts/aws_scripts/make_webserver/duck_daemon.sh /etc/rc2.d/S10duckdns

# Test again
pkill duck
sudo /etc/rc2.d/S10duckdns
ps -ef | grep duck
