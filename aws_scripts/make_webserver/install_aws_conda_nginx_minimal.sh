#https://peteris.rocks/blog/deploy-flask-apps-using-anaconda-on-ubuntu-server/
sudo apt-get update -y

sudo apt install build-essential -y

sudo mkdir -p /opt
sudo chown $USER /opt

sudo mkdir -p /opt/apps/app

conda create -n app python -y
source activate app
sudo apt-get update -y
sudo apt-get upgrade -y
conda install scipy numpy flask flask-cors -y 
conda install -c conda-forge xgboost -y
conda install PySide -y
conda install -c conda-forge uwsgi -y



curl localhost:8080
source deactivate

sudo cp uwsgi.ini /opt/apps/app
sudo cp app.py /opt/apps/app
sudo cp app.service /etc/systemd/system/app.service 


cd /opt/apps/app
uwsgi --ini uwsgi.ini
cd -


sudo systemctl start app.service
sudo systemctl restart app.service
sudo apt install -y nginx
sudo cp default /etc/nginx/sites-available/default 
sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/
sudo nginx -t

sudo systemctl restart nginx
sudo ufw allow 'Nginx Full'
sudo systemctl restart app.service
sudo mkdir /opt/apps/app/data
sudo mkdir /opt/apps/app/templates
sudo cp index.html /opt/apps/app/templates/index.html
bash restart.sh
curl localhost

# Add to autostart on boot
sudo ln -s ~/install_scripts/aws_scripts/make_webserver/nginx_daemon.sh /etc/rc2.d/S10nginx_restart
sudo /etc/rc2.d/S10nginx_restart
