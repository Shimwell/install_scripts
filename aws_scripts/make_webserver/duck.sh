#!/bin/bash

source ~/aws/aws_duck.info

current=""
while true; do
	latest=`ec2metadata --public-ipv4`
	echo "public-ipv4=$latest"
	if [ "$current" == "$latest" ]
	then
		echo "ip not changed"
	else
		echo "ip has changed - updating"
		current=$latest
		echo url="https://www.duckdns.org/update?domains="$domain"&token="$token"&ip=" | curl -k -o ~/duckdns/duck.log -K -
	fi
	sleep 5m
done