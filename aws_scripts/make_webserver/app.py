import scipy, numpy
from flask import Flask
from flask import Flask, jsonify, render_template, request, Response
from flask_cors import CORS, cross_origin

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello world you reached the main path\n'

@app.route('/helloworld')
def helloworld():
    return 'Hello world you found a sub function'

@app.route('/hello/<name>')
def hello(name):
    return 'Hello '+name+' you found a function with arguments'

@app.route('/index')
def index():
    return render_template('index.html')


if __name__ == '__main__':
    app.run()
    