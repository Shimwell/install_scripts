#!/bin/bash

#prior to running this script you should sign up to https://www.duckdns.org
#copy your unique token id and your new domain to a aws_duck.info
#your file should look something like this
#token=7d9dca40-ag5f-47b2-95ec-d7a6fc567e88
#domain=example.duckdns.org

source ~/aws/aws_vars.info
source ~/aws/aws_duck.info

echo "domain = "$domain
echo "token = "$token

echo "Install of python2 via ssh starting"
ssh -o StrictHostKeyChecking=no -i ~/.ssh/devenv-key.pem ubuntu@$aws_ip '\
git clone https://gitlab.com/Shimwell/install_scripts.git; \
cd ~/install_scripts/; \
git pull; \
bash install_python2.sh\
'
echo "Install python2 via ssh complete"

echo "Install of webserver via ssh starting"
ssh -o StrictHostKeyChecking=no -i ~/.ssh/devenv-key.pem ubuntu@$aws_ip '\
conda config --add channels conda-forge; \
cd ~/install_scripts/aws_scripts/make_webserver/; \
bash install_aws_conda_nginx_minimal.sh \
'
echo "Install of webserver via ssh complete"

echo "Set up dynamic DNS via ssh starting"
ssh -o StrictHostKeyChecking=no -i ~/.ssh/devenv-key.pem ubuntu@$aws_ip "\
mkdir ~/aws; \
cd ~/aws; \
echo "domain="$domain > aws_duck.info; \
echo "token="$token >> aws_duck.info; \
cd ~/install_scripts/aws_scripts/make_webserver/; \
bash duck_setup.sh \
"
echo "Set up dynamic DNS via ssh complete"
curl $domain
