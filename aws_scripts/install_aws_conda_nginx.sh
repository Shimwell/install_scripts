#https://peteris.rocks/blog/deploy-flask-apps-using-anaconda-on-ubuntu-server/
sudo apt-get update

sudo apt install build-essential -y

sudo mkdir -p /opt
sudo chown $USER /opt

wget https://repo.continuum.io/archive/Anaconda2-4.4.0-Linux-x86_64.sh
bash Anaconda2-4.4.0-Linux-x86_64.sh -b -p /opt/anaconda2
rm Anaconda*

echo 'export PATH="/opt/anaconda2/bin:$PATH"' >> ~/.bashrc
bash

conda --version
#conda config --add channels conda-forge

mkdir -p /opt/apps/app

wget -O environment.yml http://jshimwell.com/ssh/environment.yml

conda env create -f environment.yml -n app
source activate app

sudo apt-get update
sudo apt-get upgrade


conda install PySide -y
conda install flask-cors -y

cd /opt/apps/app
wget -O uwsgi.ini http://jshimwell.com/ssh/uwsgi.ini
wget -O app.py http://jshimwell.com/ssh/app.pyrename
uwsgi --ini uwsgi.ini

curl localhost:8080
source deactivate

sudo wget -O /etc/systemd/system/app.service http://jshimwell.com/ssh/app.service

sudo systemctl start app.service
sudo systemctl restart app.service

sudo apt install -y nginx

sudo wget -O /etc/nginx/sites-available/default http://jshimwell.com/ssh/default

sudo ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/

sudo nginx -t

sudo systemctl restart nginx

sudo ufw allow 'Nginx Full'

wget -O restart.sh http://jshimwell.com/ssh/restart.sh

sudo systemctl restart app.service

mkdir data
mkdir templates

curl localhost





