#!/bin/bash

source ~/aws/aws_vars.info
aws ec2 stop-instances  --instance-ids $aws_id

aws ec2 describe-instances
