#!/bin/bash

source ~/aws/aws_sg.info

aws_id=$(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --query 'Reservations[0].Instances[0].InstanceId')
echo 'aws_id='$aws_id > ~/aws/aws_vars.info

aws_ip=$(aws ec2 describe-instances --instance-ids $aws_id --query 'Reservations[0].Instances[0].PublicIpAddress')
echo 'aws_ip='$aws_ip >> ~/aws/aws_vars.info
