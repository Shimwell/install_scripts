#!/bin/bash

source ~/aws/aws_sg.info

aws_id=$(aws ec2 run-instances --image-id ami-996372fd --security-group-ids $aws_sg --count 1 --instance-type t2.micro --key-name devenv-key --query 'Instances[0].InstanceId')
echo 'aws_id='$aws_id > ~/aws/aws_vars.info

aws_ip=$(aws ec2 describe-instances --instance-ids $aws_id --query 'Reservations[0].Instances[0].PublicIpAddress')
echo 'aws_ip='$aws_ip >> ~/aws/aws_vars.info
