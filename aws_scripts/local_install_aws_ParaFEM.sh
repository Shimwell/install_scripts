#!/bin/bash
source ~/aws/aws_vars.info
echo $aws_id
echo $aws_ip
ssh -o StrictHostKeyChecking=no -i ~/.ssh/devenv-key.pem ubuntu@$aws_ip 'mkdir ~/install_scripts; wget -O ~/install_scripts/install_parafem.sh https://gitlab.com/Shimwell/install_scripts/raw/master/install_parafem.sh; chmod 755 ~/install_scripts/install_parafem.sh; bash ~/install_scripts/install_parafem.sh; source ~/.bashrc'
