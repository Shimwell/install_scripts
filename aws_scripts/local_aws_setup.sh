# downloads and installs AWSCLI commands
conda install -c conda-forge awscli -y

# Create new user via aws web portal
# Services/IAM/users/Add user
# User name, acces type=all, create password, uncheck password reset,Attach existing policies directly=AdministratorAccess,create user
# For credentials select 'user' then 'Security Credentials'

# configures AWSCLI
aws configure
#AWS Access Key ID [None]: Get from Services/IAM/users/user_name
#AWS Secret Access Key [None]: ****
#Default region name [None]: eu-west-2
#Default output format [None]: text

# makes a security group for future
aws ec2 create-security-group --group-name devenv-sg --description "EC2 dev"
#Get groupID
aws_sg=$(aws ec2 describe-security-groups --filters Name=group-name,Values='*devenv*' --query 'SecurityGroups[*].{ID:GroupId}')
#Place groupID in file
mkdir -p ~/aws
echo 'aws_sg='$aws_sg > ~/aws/aws_sg.info

aws ec2 authorize-security-group-ingress --group-name devenv-sg --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name devenv-sg --protocol tcp --port 80 --cidr 0.0.0.0/0

# makes a ssh key
mkdir -p ~/.ssh
cd ~/.ssh
aws ec2 create-key-pair --key-name devenv-key --query 'KeyMaterial' --output text > devenv-key.pem
chmod 400 ~/.ssh/devenv-key.pem
