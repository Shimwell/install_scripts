#!/bin/bash

sudo apt-get update
sudo apt-get install -y linux-headers-generic
sudo apt install build-essential -y

sudo apt-get install -y libglu1-mesa
sudo apt-get install -y libsm6
sudo apt-get install -y libxt6
#sudo apt-get install libxext6

sudo chown $USER /opt
mkdir /opt/paraview
cd /opt/paraview
wget -O paraview.tar.gz "https://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v5.4&type=binary&os=Linux&downloadFile=ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit.tar.gz"
tar xf paraview.tar.gz
rm paraview.tar.gz

echo 'PATH="'"$PATH"':/opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/bin"' | sudo tee /etc/environment

cd ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/

echo '#!/bin/bash' > run_Visualizer.sh
echo "" >> run_Visualizer.sh
echo "cd /opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit" >> run_Visualizer.sh
echo './bin/pvpython   \' >> run_Visualizer.sh
echo '    ./share/paraview-5.4/web/visualizer/server/pvw-visualizer.py  \' >> run_Visualizer.sh
echo '    --content ./share/paraview-5.4/web/visualizer/www/             \' >> run_Visualizer.sh
echo '    --data $PWD/share/paraview-5.4/data                         \' >> run_Visualizer.sh
echo '    --port 8080' >> run_Visualizer.sh

chmod 777 /opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/run_Visualizer.sh 
ln -s /opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/run_Visualizer.sh /opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/bin/run_Visualizer

# run_Visualizer

# add models to folder /opt/paraview/ParaView-5.4.1-Qt5-OpenGL2-MPI-Linux-64bit/share/paraview-5.4/data
