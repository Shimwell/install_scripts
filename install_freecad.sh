sudo add-apt-repository ppa:freecad-maintainers/freecad-stable
sudo add-apt-repository ppa:freecad-maintainers/freecad-daily
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install freecad-daily

#needed for import MeshPart to work
conda install libgcc